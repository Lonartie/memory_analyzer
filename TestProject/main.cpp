#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include <QString>

#include <QtGui/QImage>
#include <QtCore/QSize>

#include <windows.h>
using uchar = unsigned char;

#define _MAIN

std::ostream& operator<<(std::ostream& stream, const QImage& img)
{
	stream << "width: " << img.width() << " height: " << img.height();
   return stream;
}

template<typename T>
std::ostream& operator<<(std::ostream& stream, const std::vector<T>& vec)
{
	stream << "{ ";
	for (int i = 0; i < vec.size(); i++)
		stream << vec[i] << (i == vec.size() - 1 ? "" : ", ");
	stream << " }";
	return stream;
}

template<>
std::ostream& operator<<(std::ostream& stream, const std::vector<unsigned char>& vec)
{
	stream << "{ ";
	for (int i = 0; i < vec.size(); i++)
		stream << (int)vec[i] << (i == vec.size() - 1 ? "" : ", ");
	stream << " }";
	return stream;
}

template<typename T>
std::stringstream& operator<<(std::stringstream& stream, const std::vector<T>& vec)
{
	stream << "{ ";
	for (int i = 0; i < vec.size(); i++)
		stream << vec[i] << (i == vec.size() - 1 ? "" : ", ");
	stream << " }";
	return stream;
}

std::string align(const std::string& str, int size)
{
	std::string out;
	for (int i = str.length(); i < size; i++)
		out += " ";
	return out;
}

template<typename T>
std::string memory(T* o)
{
	std::string out;
	for (long long i = 0; i < sizeof(T); i++)
		out += align(std::to_string((int) (*(((uchar*) o) + i))), 3) + std::to_string((int) (*(((uchar*) o) + i))) + " ";
	return out;
}

#define PRINT(x) std::cout << #x << ": " << align(#x, 15) << x << align((std::stringstream() << x).str(), 25) << " 0x" << &x /*<< "\t" << memory(&x)*/ << std::endl;

#ifdef _MAIN

int main(int argc, char *argv[])
{
	int _int = 123;
	double _double = 1.23;
	float _float = 1.23f;
   std::vector<int> _vector_int = {1, 2, 3};
   std::vector<double> _vector_double = {1.1, 2.2, 3.3};
   std::vector<float> _vector_float = {1.1f, 2.2f, 3.3f};

	QImage _qimage(QSize(500, 500), QImage::Format_ARGB32);
	_qimage.fill(QColor(255, 255, 0, 128));

	PRINT(_int);
	PRINT(_double);
	PRINT(_float);
   PRINT(_vector_int);
   PRINT(_vector_double);
   PRINT(_vector_float);
   PRINT(_qimage);
	
	std::cout << std::endl << "Executable: " << argv[0] << std::endl;
	std::cout << "Process ID: " << GetCurrentProcessId() << std::endl << std::endl;

	std::cin.get();

	std::string _test = "hallo";
	std::cout << (char*)&_test << std::endl;

	//std::cout << "filled qimage green" << std::endl;
	system("pause");
}

#else

template<typename T, typename D>
long long offset_of(T* obj, D* target)
{
	std::size_t a0 = reinterpret_cast<std::size_t>(static_cast<void*>(target));
	void* _a0 = &a0;

	std::vector<uchar> ab((uchar*) obj, (uchar*) obj + sizeof(T));
	std::vector<uchar> a0b((uchar*) _a0, (uchar*) _a0 + sizeof(_a0));

	for (std::size_t offset = 0; offset < (ab.size() - a0b.size()); offset++)
	{
		bool equal = true;
		for (std::size_t i = 0; i < a0b.size(); i++)
			if (ab[offset + i] != a0b[i])
			{
				equal = false;
				break;
			}
		if (equal)
			return offset;
	}
	return -1;
}

int main()
{
   std::vector<int> u = {1, 2, 3};
   std::cout << &u << " | " << &u[0] << std::endl;

	constexpr auto size = sizeof(u);

	std::cout << offset_of(&u, &u[0]) << std::endl;

	system("Pause");
}

#endif