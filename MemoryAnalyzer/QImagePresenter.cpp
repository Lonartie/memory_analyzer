#include "QImagePresenter.h"
#include "MemoryFormatter.h"
#include "MemoryController.h"
#include "QtPrivateInformation.h"
#include "ImageView.h"
#include "MemoryMapParser.h"

#include <QImage>
#include <QLabel>
#include <QPixmap>
#include <QGridLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
//#include <QtGUi/5.13.0/QtGui/private/qimage_p.h>

using namespace Debugger;

namespace
{
   std::vector<uchar> range(uchar* data, std::size_t offset, std::size_t size)
   {
      return std::vector<uchar>(data + offset, data + offset + size);
   }
}

QString QImagePresenter::getName() const
{
   return "QImage";
}

QString QImagePresenter::getGroup() const
{
   return "Qt";
}

QWidget* QImagePresenter::getWidget(Debugger::Process proc, void* address) const
{
   if (GetQtVersion().major == 0)
   {
      if (QMessageBox::Abort == QMessageBox::critical(0,
                                                      "Could not find qt version",
                                                      "The Qt version of the process could not be determined..\n"
                                                      "Reading anyway? (could lead to crash!)",
                                                      QMessageBox::Ignore | QMessageBox::Abort,
                                                      QMessageBox::Abort))
      {
         return 0;
      }
   }

   QString size = QString::number(QImagePrivate::size());
   QString ow = QString::number(QImagePrivate::offset_width());
   QString oh = QString::number(QImagePrivate::offset_height());
   QString obpl = QString::number(QImagePrivate::offset_bytes_per_line());
   QString of = QString::number(QImagePrivate::offset_format());
   QString od = QString::number(QImagePrivate::offset_data());
   QList<MemoryMap> l1 =
   {
      {"QImage", OBJECT, "input", "0", "32"},
      {"PrivatePtr", POINTER, "QImage", "24", "8"},
      {"PrivateObj", OBJECT, "PrivatePtr", "0", size},
      {"Width", OBJECT, "PrivateObj", ow, "4"},
      {"Height", OBJECT, "PrivateObj", oh, "4"},
      {"BPL", OBJECT, "PrivateObj", obpl, "8"},
      {"Format", OBJECT, "PrivateObj", of, "4"},
   };

   auto s1 = MemoryMapParser::ParseMemoryMaps(l1, address, proc);
   int width = MemoryMapParser::GetValue<int>(s1, "Width");
   int height = MemoryMapParser::GetValue<int>(s1, "Height");
   int bpl = MemoryMapParser::GetValue<std::size_t>(s1, "BPL");
   int format = MemoryMapParser::GetValue<int>(s1, "Format");

   QString os = QString::number(bpl * height);
   QList<MemoryMap> l2 =
   {
      {"QImage", OBJECT, "input", "0", "32"},
      {"PrivatePtr", POINTER, "QImage", "24", "8"},
      {"PrivateObj", OBJECT, "PrivatePtr", "0", size},
      {"DataPtr", POINTER, "PrivateObj", od, "8"},
      {"Data", OBJECT, "DataPtr", "0", os},
   };

   auto s2 = MemoryMapParser::ParseMemoryMaps(l2, address, proc);
   auto vec = MemoryMapParser::GetValue<uchar*>(s2, "Data");

   QImage img(&vec[0], width, height, bpl, static_cast<QImage::Format>(format));
   auto view = new ImageView;
   view->setImage(img.copy());

   return view;
}

REGISTER_PRESENTER(QImagePresenter);
