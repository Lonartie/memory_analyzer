#include "MemorySnapshotAnalyzer.h"
#include "MemoryFormatter.h"

#include <exception>

MemoryBlocks MemorySnapshotAnalyzer::getMemoryBlocks(const GlobalDifferences& diff)
{
   MemoryBlocks result;

   std::size_t ind = 0;
   for (auto& module : diff.modules)
   {
      MemoryBlock current;

      bool last_state = false;
      bool end_found = true;
      std::size_t begin_offset = 0;
      std::size_t end_offset = 0;
      for (std::size_t offset = 0; offset < module.diffs.size(); offset++)
      {
         if (module.diffs[offset] != last_state)
         {
            if (!last_state)
            {
               // block begin
               begin_offset = offset;
               current.mod_index = ind;
               current.mod_offset = begin_offset;
               current.begin = module.after.begin + offset;
               end_found = false;
            } else
            {
               // block end (+1)
               end_offset = offset;
               current.end = module.after.begin + end_offset;
               current.size = current.end - current.begin;
               current.data = std::vector<unsigned char>(module.after.data.begin() + begin_offset, module.after.data.begin() + end_offset);
               end_found = true;
               result.push_back(current);
               current = {};
            }
            last_state = module.diffs[offset];
            continue;
         }
      }

      // if block goes until end it will be captured here
      if (!end_found && last_state)
      {
         // block end (+1)
         end_offset = module.diffs.size();
         current.end = module.after.begin + end_offset;
         current.size = current.end - current.begin;
         current.data = std::vector<unsigned char>(
            module.after.data.begin() + begin_offset,
            module.after.data.begin() + end_offset
            );
         end_found = true;
         result.push_back(current);
         current = {};
      }
      ind++;
   }

   return mergeThreshold(result, diff.fallback, 64);
}

MemoryBlockPointers MemorySnapshotAnalyzer::getMemoryBlockPointers(const MemoryBlocks& blocks, std::function<void(float progress)> progressFunc /*= nullptr*/)
{
   MemoryBlockPointers result;

   std::size_t checks = 0, checks_done = 0;
   for (auto& current : blocks)
      for (auto& reference : blocks)
         if (current.size >= 8)
            checks += (current.size - 8);


	int last_progress = 0;
   for (auto& current : blocks)
   {
      for (auto& reference : blocks)
      {
         if (current.size < 8) continue;

         auto csize = current.size;
         auto rsize = reference.size;

         for (std::size_t coffset = 0; coffset < csize - 8; coffset++)
         {
            std::vector<unsigned char> potential_pointer_raw(
               current.data.begin() + coffset,
               current.data.begin() + coffset + 8
            );
            auto potential_pointer = MemoryFormatter::formatVec<std::size_t>(potential_pointer_raw)[0];

				checks_done++;
				int progress = checks_done / float(checks) * 100;
				if (progressFunc && progress != last_progress)
				{
					progressFunc(progress / 100.0f);
					last_progress = progress;
				}

				if (potential_pointer >= reference.begin - 32 && potential_pointer <= reference.end + 32)
				{
					MemoryBlockPointer pointer;
					pointer.source = &current;
					pointer.target = &reference;
					pointer.source_offset = coffset;
					pointer.target_offset = potential_pointer - reference.begin;
					pointer.parsed_pointer = potential_pointer;
					result.push_back(pointer);
				}
         }
      }
   }

   return result;
}

MemoryBlockChains MemorySnapshotAnalyzer::getPointerPaths(const MemoryBlockPointers& pointers, const MemoryBlocks& blocks, std::size_t begin, std::size_t end)
{
   // were begin and end pointers recognized in blocks?
   const MemoryBlock* begin_block = nullptr, * end_block = nullptr;
   for (auto& block : blocks)
   {
      if (block.begin <= begin && block.end >= begin) begin_block = &block;
      if (block.begin <= end && block.end >= end) end_block = &block;
   }
   if (!begin_block) throw std::exception("begin not found");
   if (!end_block) throw std::exception("end not found");

   // same block!
   if (begin_block == end_block) throw std::exception("begin and end are in same block!");

   // are there any pointers going out of begin or into end?
   std::vector<const MemoryBlockPointer*> begin_pointers, end_pointers;
   for (auto& pointer : pointers)
   {
      if (pointer.source == begin_block) begin_pointers.push_back(&pointer);
      if (pointer.target == end_block) end_pointers.push_back(&pointer);
   }
   if (!begin_pointers.size()) throw std::exception(QString("no pointer going out of begin block, size before: %1, size after: %2")
																	 .arg(begin - begin_block->begin)
																	 .arg(begin_block->end - (begin + 8))
																	 .toStdString().c_str());
   if (!end_pointers.size()) throw std::exception("no pointer pointing to end block");

   // if we made it here, there is a chance a path exists
   std::vector<MemoryBlockChain*> results;
   for (auto& begin_pointer : begin_pointers)
   {
      auto chain_begin = new MemoryBlockChain;
      chain_begin->offset = begin_pointer->source->begin + begin_pointer->source_offset - begin;
      chain_begin->source = begin_pointer->source;
      chain_begin->target = begin_pointer->target;
      chain_begin->pointer = begin_pointer;

      findRecursively({blocks, pointers},
                      {begin_block, end_block},
                      {begin_pointers, end_pointers},
                      chain_begin,
                      results);

      delete chain_begin;
   }

   return results;
}

MemoryBlocks& MemorySnapshotAnalyzer::mergeThreshold(MemoryBlocks& blocks, const GlobalSnapshot& fallback, std::size_t threshold)
{
   std::sort(blocks.begin(), blocks.end(), [](auto& a, auto& b) { return a.begin < b.begin; });

   std::size_t index = 0;
   // merge
   while (true)
   {
      if (!(index < blocks.size() - 1))
         break;

      auto cindex = index;
      index++;

      auto current = blocks[cindex];
      auto next = blocks[cindex + 1];

      auto space = next.begin - current.end;
      if (space > threshold)
         continue;

      auto& fallback_data = fallback.modules[current.mod_index].data;
      std::vector<uchar> between = std::vector<uchar>
      (
         fallback_data.begin() + current.mod_offset + current.size,
         fallback_data.begin() + current.mod_offset + current.size + space
      );

      auto& merged = current;
      merged.size += space + next.size;
      merged.data.insert(merged.data.end(), between.begin(), between.end());
      merged.data.insert(merged.data.end(), next.data.begin(), next.data.end());
      merged.end = next.end;

      blocks[cindex] = merged;
      blocks.erase(blocks.begin() + cindex + 1);
   }

   // expand
   for (auto& block : blocks)
	{
		auto& mod = fallback.modules[block.mod_index];
		auto& fallback_data = mod.data;
		auto first_size = std::min(threshold / 2, block.mod_offset);
		auto last_size = std::min(threshold / 2, mod.begin + mod.size - block.end);

		// expand begin
		if (first_size > 0)
		{
			block.begin -= first_size;
			std::vector<uchar> first = std::vector<uchar>
			(
				fallback_data.begin() + block.mod_offset - first_size,
				fallback_data.begin() + block.mod_offset
			);
			block.mod_offset -= first_size;
			block.data.insert(block.data.begin(), first.begin(), first.end());
		}

		// expand end
		if (last_size > 0)
		{
			block.end += last_size;
			std::vector<uchar> last = std::vector<uchar>
			(
				fallback_data.begin() + block.mod_offset,
				fallback_data.begin() + block.mod_offset + last_size
			);
			block.data.insert(block.data.begin(), last.begin(), last.end());
		}

		block.size += first_size + last_size;
   }

   return blocks;
}

void MemorySnapshotAnalyzer::findRecursively(BaseData base, BlockFrame bframe, PointersFrame pframe, MemoryBlockChain* current, MemoryBlockChains& results, std::vector<const MemoryBlock*> processed /*= {}*/)
{
   // if we jumped up from a failed path, clear up
   if (current->next) delete current->next;

   // specify source and target
   auto source = current->source;
   auto target = current->target;

   // if we found the end, exit
   if (target == bframe.end)
   {
      results.push_back(current->deepClone());
      return;
   }

   // if we have a loop, exit
   for (auto& proc : processed) if (source == proc) return;
   processed.push_back(source);

   // find all pointers going out of target block
   for (auto& next_ptr : base.pointers)
   {
      if (next_ptr.source == target)
      {
         // found pointer going out of target block
         auto next = new MemoryBlockChain;
         next->offset = next_ptr.source->begin + next_ptr.source_offset - current->pointer->parsed_pointer;
         next->source = next_ptr.source;
         next->target = next_ptr.target;
         next->pointer = &next_ptr;
         current->next = next;

         findRecursively(base, bframe, pframe, next, results, processed);

			delete next; 
			current->next = nullptr;
      }
   }
}
