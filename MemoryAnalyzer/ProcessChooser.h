#pragma once

#include "ui_ProcessChooser.h"

#include <QDialog>
#include <QWidget>

class ProcessChooser: public QDialog
{
   Q_OBJECT

public:

   static std::size_t Choose(QWidget* parent = Q_NULLPTR);
   
private:
   ProcessChooser(QWidget* parent = Q_NULLPTR);
   ~ProcessChooser() = default;

private slots:

   void on__select_pressed();
   void on__search_textChanged(const QString& text);

private:

   void loadTable();

   std::vector<std::size_t> pids;
   std::size_t value = 0;
   Ui::ProcessChooser ui;
};
