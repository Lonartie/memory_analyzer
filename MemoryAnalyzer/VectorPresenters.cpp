#include "VectorPresenters.h"
#include "MemoryController.h"
#include "MemoryFormatter.h"
#include "MemoryMapParser.h"

#include <QTableWidget>
#include <QHeaderView>

namespace
{
	template<typename T>
	int begin_offset()
	{
		std::vector<T> tmp = {0, 1, 2};		
		return MemoryFormatter::offsetOf(&tmp, &tmp[0]);
	}
	template<typename T>
	int end_offset()
	{
		std::vector<T> tmp = {0, 1, 2};
		return MemoryFormatter::offsetOf(&tmp, &tmp[2] + 1);
	}
	template<typename T>
	QWidget* table(const std::vector<T>& vec)
	{
		auto table = new QTableWidget();
		table->horizontalHeader()->hide();
		table->setRowCount(vec.size());
		table->setColumnCount(1);
		int i = 0;
		for (const auto& item : vec)
			table->setItem(i++, 0, new QTableWidgetItem(QString::number(item)));
		return table;
	}
}

#define VECTOR_PRESENTER_DEFINITION(NAME, BASE_TYPE)										\
QString NAME::getName() const																		\
{ return QString("std::vector<%1>").arg(#BASE_TYPE); }									\
QString NAME::getGroup() const																	\
{ return "STL"; }																						\
QWidget* NAME::getWidget(Debugger::Process proc, void* address) const				\
{																											\
	auto str = [](std::size_t x) {return QString::number(x); };							\
	QList<MemoryMap> maps =																			\
	{																										\
      {"BeginPtr", POINTER, "input", str(begin_offset<BASE_TYPE>()), "8"},			\
      {"EndPtr", POINTER, "input", str(end_offset<BASE_TYPE>()), "8"},				\
		{"Data", OBJECT, "BeginPtr", 0, "EndPtr"}												\
	};																										\
	auto settings = MemoryMapParser::ParseMemoryMaps(maps, address, proc);			\
	auto data = MemoryMapParser::GetValue<BASE_TYPE*>(settings, "Data");				\
																											\
	return table(data);																				\
}																											\
REGISTER_PRESENTER(NAME);


VECTOR_PRESENTER_DEFINITION(VectorIntPresenter, int);
VECTOR_PRESENTER_DEFINITION(VectorUIntPresenter, unsigned int);
VECTOR_PRESENTER_DEFINITION(VectorLongPresenter, long);
VECTOR_PRESENTER_DEFINITION(VectorULongPresenter, unsigned long);
VECTOR_PRESENTER_DEFINITION(VectorLongLongPresenter, long long);
VECTOR_PRESENTER_DEFINITION(VectorULongLongPresenter, unsigned long long);
VECTOR_PRESENTER_DEFINITION(VectorCharPresenter, char);
VECTOR_PRESENTER_DEFINITION(VectorUCharPresenter, unsigned char);
VECTOR_PRESENTER_DEFINITION(VectorDoublePresenter, double);
VECTOR_PRESENTER_DEFINITION(VectorFloatPresenter, float);