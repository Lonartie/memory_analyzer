#include "AdvancedLabel.h"

AdvancedLabel::AdvancedLabel(QWidget* parent)
   : QLabel(parent)
{
   setMinimumSize(1, 1);
}

void AdvancedLabel::setImage(const QImage& image)
{
   m_image = image;
   __resize(size(), image);
   m_resizeEnabled = true;
}

void AdvancedLabel::enableZoom(bool enable)
{
   m_enableZoom = enable;
}

void AdvancedLabel::resizeEvent(QResizeEvent* event)
{
   if (!m_resizeEnabled) return;

   if (!m_image.isNull())
      __resize(event->size(), m_image);
}

void AdvancedLabel::mousePressEvent(QMouseEvent* ev)
{
   m_drag = true;
   m_mouse = ev->pos();
}

void AdvancedLabel::mouseMoveEvent(QMouseEvent* ev)
{
   if (!m_drag) return;

   auto delta = ev->pos() - m_mouse;

   m_offsetx += delta.x();
   m_offsety += delta.y();

   m_mouse = ev->pos();

   __resize(size(), m_image);
}

void AdvancedLabel::mouseReleaseEvent(QMouseEvent* ev)
{
   m_drag = false;
}

void AdvancedLabel::wheelEvent(QWheelEvent* event)
{
   if (!m_enableZoom) return;

   auto delta = event->angleDelta().y();

   if (delta > 0)
   {
      m_zoom *= 1.15;
   } else
   {
      m_zoom /= 1.15;
   }

   __resize(size(), m_image);
}

void AdvancedLabel::__resize(const QSize& size, const QImage& image)
{
   if (size.width() < image.width() || size.height() < image.height())
      setPixmap(QPixmap::fromImage(image.scaled(size * m_zoom, Qt::KeepAspectRatio)));
   else
      setPixmap(QPixmap::fromImage(image.scaled(image.size() * m_zoom)));
}

