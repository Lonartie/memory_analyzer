#include "MemoryController.h"

#include <stdexcept>
#include <algorithm>
#include <sstream>

#include <windows.h>
#include <Psapi.h>

#pragma comment(lib, "kernel32.lib")

#undef max

namespace Debugger
{
	void MemoryController::enableDebugMode()
	{
		HANDLE mainToken;

		// I really don't know what this block of code does :<
		if (!OpenThreadToken(GetCurrentThread(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, FALSE, &mainToken))
		{
			if (GetLastError() == ERROR_NO_TOKEN)
			{
				if (!ImpersonateSelf(SecurityImpersonation))
					throw std::runtime_error("error impersonating, code: " + std::to_string(GetLastError()));

				if (!OpenThreadToken(GetCurrentThread(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, FALSE, &mainToken))
					throw std::runtime_error("error opening thread token, code: " + std::to_string(GetLastError()));

			}
			else
				throw std::runtime_error("error unknown location, code: " + std::to_string(GetLastError()));
		}

		try
		{
			set_privilege(mainToken, SE_DEBUG_NAME, true);
		}
		catch (...)
		{
			CloseHandle(mainToken);
			throw;
		}
	}

	std::vector<Debugger::MemoryModule> MemoryController::getModules(Process process, bool skipUnused /*= true*/)
	{
		std::vector<MemoryModule> result;

		unsigned char* p = NULL;
		MEMORY_BASIC_INFORMATION info;

		for (p = NULL;
			  VirtualQueryEx(process.handle, p, &info, sizeof(info)) == sizeof(info);
			  p += info.RegionSize)
		{
			MemoryModule current;

			current.base = info.BaseAddress;
			current.size = info.RegionSize;

			switch (info.State)
			{
			case MEM_COMMIT:
				current.state = State::Commit;
				break;
			case MEM_RESERVE:
				current.state = State::Reserve;
				break;
			case MEM_FREE:
				current.state = State::Free;
				break;
			}

			if (skipUnused && current.state != State::Commit) continue;

			switch (info.Type)
			{
			case MEM_IMAGE:
				current.type = Type::Image;
				break;
			case MEM_MAPPED:
				current.type = Type::Mapped;
				break;
			case MEM_PRIVATE:
				current.type = Type::Private;
			}

			if (skipUnused && current.type != Type::Private) continue;

			current.nocache = (info.AllocationProtect & PAGE_NOCACHE) == PAGE_NOCACHE;
			current.guard = (info.AllocationProtect & PAGE_GUARD) == PAGE_GUARD;
			info.AllocationProtect &= ~(PAGE_GUARD | PAGE_NOCACHE);

			if (skipUnused && current.guard) continue;

			switch (info.AllocationProtect)
			{
			case PAGE_READONLY:
				current.protection = Protection::Read;
				break;
			case PAGE_READWRITE:
				current.protection = Protection::ReadWrite;
				break;
			case PAGE_WRITECOPY:
				current.protection = Protection::WriteCopy;
				break;
			case PAGE_EXECUTE:
				current.protection = Protection::Execute;
				break;
			case PAGE_EXECUTE_READ:
				current.protection = Protection::ExecuteRead;
				break;
			case PAGE_EXECUTE_READWRITE:
				current.protection = Protection::ExecuteReadWrite;
				break;
			case PAGE_EXECUTE_WRITECOPY:
				current.protection = Protection::ExecuteWriteCopy;
				break;
			}

			result.push_back(current);
		}

		return result;
	}

	void* MemoryController::getBaseAddress(Process proc)
{
		HMODULE hMods[1024];
		HANDLE pHandle = proc.handle;
		DWORD cbNeeded;
		unsigned int i;

		if (EnumProcessModules(pHandle, hMods, sizeof(hMods), &cbNeeded))
		{
			for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
			{
				TCHAR szModName[MAX_PATH];
				if (GetModuleFileNameEx(pHandle, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR)))
				{
					std::wstring wstrModName = szModName;
					std::string strModName(wstrModName.begin(), wstrModName.end());
					std::string strModContain = processName(proc);
					if (strModName.find(strModContain) != std::string::npos)
					{
						return hMods[i];
					}
				}
			}
		}
		return nullptr;
	}

	bool MemoryController::addressInModules(const std::vector<MemoryModule>& modules, const void* adr)
	{
		return std::find_if(modules.begin(), modules.end(), [=](auto& mod)
		{
			return toNum(adr) >= toNum(mod.base) && toNum(adr) <= toNum(as<char>(mod.base) + mod.size);
		}) != modules.end();
	}

	std::vector<char> MemoryController::readMod(Process proc, const MemoryModule& info)
	{
		return read<char>(proc, info.base, info.size);
	}

	std::vector<std::size_t> MemoryController::currentProcesses()
	{
		std::vector<std::size_t> result;
		constexpr auto size = std::numeric_limits<short>::max();
		DWORD *processes = nullptr, needed, count;
		processes = new DWORD[size];

		if (!EnumProcesses(processes, size, &needed))
		{
			delete[] processes;
			return {};
		}

		count = needed / sizeof(DWORD);

		result.resize(count);

#pragma omp parallel for
		for (long long i = 0; i < count; i++)
			result[i] = processes[i];

		delete[] processes;
		return result;
	}

	std::size_t MemoryController::processOf(const void* addr)
	{
		auto pids = currentProcesses();

		for (auto& pid : pids)
		{
			auto process = openProcess(pid);
			if (!isValid(process)) continue;

			auto modules = getModules(process);

			if (addressInModules(modules, addr))
			{
				closeProcess(process);
				return pid;
			}

			closeProcess(process);
		}

		return 0;
	}

	std::string MemoryController::processName(Process process)
	{
		TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
		HANDLE hProcess = process.handle;

		if (NULL != hProcess)
		{
			HMODULE hMod;
			DWORD cbNeeded;

			if (EnumProcessModules(hProcess, &hMod, sizeof(hMod),
										  &cbNeeded))
			{
				GetModuleBaseName(hProcess, hMod, szProcessName,
										sizeof(szProcessName) / sizeof(TCHAR));
			}
		}

		std::wstring res(szProcessName);
		return std::string(res.begin(), res.end());
	}

	std::string MemoryController::processPath(Process process)
	{
		TCHAR szFileName[MAX_PATH] = TEXT("<unknown>");
		HANDLE hProcess = process.handle;

		if (NULL != hProcess)
		{
			GetModuleFileNameEx(hProcess, NULL, szFileName, MAX_PATH);
		}

		std::wstring res(szFileName);
		return std::string(res.begin(), res.end());
	}

	bool MemoryController::isValid(Process process)
	{
		return process.pid && process.handle && process.handle != INVALID_HANDLE_VALUE;
	}

	Debugger::Process MemoryController::openProcess(std::size_t pid)
	{
		return
		{
			OpenProcess(PROCESS_VM_READ | PROCESS_QUERY_INFORMATION,false,static_cast<DWORD>(pid)),
			pid
		};
	}

	Debugger::Process MemoryController::openProcess(const std::string& name)
	{
		for (const auto& pid : currentProcesses())
		{
			auto proc = openProcess(pid);
			if (processName(proc) == name)
				return proc;
			closeProcess(proc);
		}
		return {};
	}

	bool MemoryController::closeProcess(Process proc)
	{
		return CloseHandle(proc.handle);
	}

	void* MemoryController::addressof(void* type, std::size_t offset)
	{
		return reinterpret_cast<void*>(*static_cast<std::size_t*>(static_cast<void*>(static_cast<char*>(type) + offset)));
	}

	void MemoryController::setdata(void* type, std::size_t offset, void* data)
	{
		auto adr_m = reinterpret_cast<void**>(static_cast<std::size_t*>(static_cast<void*>(static_cast<char*>(type) + offset)));;
		(*adr_m) = data;
	}

	std::pair<char*, std::size_t> MemoryController::trim(char* buffer, std::size_t size)
	{
		while (size && buffer[0] != 0) { buffer++; size--; }
		while (size && buffer[size - 1] == 0) { size--; }
		return {buffer, size};
	}

	std::vector<std::pair<char*, std::size_t>> MemoryController::split(char* buffer, std::size_t size)
	{
		auto trimmed = trim(buffer, size);

		buffer = trimmed.first;
		size = trimmed.second;

		if (!size) return {};

		std::vector<std::pair<char*, std::size_t>> result;

		std::pair<char*, std::size_t> current{buffer, 0};
		std::size_t n = 0;

		while (n < size)
		{
			if (buffer[n++] == 0)
			{
				if (current.second)
					result.push_back(current);
				current = {buffer + n, 0};
				continue;
			}

			current.second++;
		}

		return result;
	}

	std::size_t MemoryController::getAddr(const char* ptr)
	{
		return *as<const std::size_t>(ptr);
	}

	std::size_t MemoryController::getAddr(const std::vector<char>& ptr)
	{
		return getAddr(ptr.data());
	}

	void* MemoryController::getPtr(const char* ptr)
	{
		return MC::toPtr(getAddr(ptr));
	}

	void* MemoryController::getPtr(const std::vector<char>& ptr)
	{
		return getPtr(ptr.data());
	}

	std::string MemoryController::toStr(const void* addr)
	{
		return "0x" + (std::stringstream() << addr).str();
	}

	std::string MemoryController::toStr(std::size_t addr)
	{
		return toStr(toPtr(addr));
	}

	std::size_t MemoryController::lastError()
	{
		return GetLastError();
	}

	bool MemoryController::readProcessMemory(void* handle, void* addr, void* out, std::size_t size, std::size_t* read)
	{
		return ReadProcessMemory(handle, addr, out, size, read);
	}

	void MemoryController::set_privilege(void* hToken, const wchar_t* lpszPrivilege, bool bEnablePrivilege)
	{
		TOKEN_PRIVILEGES tp;
		LUID luid;

		if (!LookupPrivilegeValue(
			NULL,            // lookup privilege on local system
			lpszPrivilege,   // privilege to lookup 
			&luid))        // receives LUID of privilege
			throw std::runtime_error("error looking for the privilege, code: " + std::to_string(GetLastError()));

		tp.PrivilegeCount = 1;
		tp.Privileges[0].Luid = luid;
		if (bEnablePrivilege)
			tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		else
			tp.Privileges[0].Attributes = 0;

		if (!AdjustTokenPrivileges(
			hToken,
			FALSE,
			&tp,
			sizeof(TOKEN_PRIVILEGES),
			(PTOKEN_PRIVILEGES) NULL,
			(PDWORD) NULL))
			throw std::runtime_error("error adjusting privileges, code: " + std::to_string(GetLastError()));

		if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)
			throw std::runtime_error("token does not have this privilege");
	}

}