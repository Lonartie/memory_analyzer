#include "QtHashes.h"

#include <QCryptographicHash>
#include <QFile>
#include <QMap>

#include <QMessageBox>

#define ADD_MODE false

QMap<QString /*hash*/, QtVersion /*Version*/> VERSION_LIST = 
{
	{ "432887D366B0A567FF06EE5FB5634665", {5, 14, 0} }, // release
	{ "0ACCBFC2C3DAB0B38B8A8F33AB64904C", {5, 14, 0} }, // debug
   { "55894873E67D052FC2B1F8741FAD080B", {5, 13, 0} }, // release
   { "120D217D4AC248502D7F188B6C9C9253", {5, 13, 0} }, // debug
	{ "213A6D4B5499E5E563ECE6A1AA0653D4", {5, 12, 4} }, // release
   { "2FE18556848D3AF1AB03857B73399801", {5, 12, 4} }, // debug
};

QtVersion QtHashes::GetVersion(const QString& QtCoreDllPath)
{
	QFile file(QtCoreDllPath);
	if (!file.open(QIODevice::ReadOnly)) return {};
	QCryptographicHash hash(QCryptographicHash::Algorithm::Md5);
	if (!hash.addData(&file)) return {};
	QString md5_hash(hash.result().toHex().toUpper());

	if (VERSION_LIST.keys().contains(md5_hash))
		return VERSION_LIST[md5_hash];

	if (ADD_MODE)
	{
		QMessageBox::information(0, "New qt version", QString("file: %1\nhash: %2").arg(QtCoreDllPath).arg(md5_hash));
	}

	return {};
}
