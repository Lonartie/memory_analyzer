#pragma once

#include "MemoryMap.h"
#include "Structs.h"
#include "MemoryController.h"

#include <vector>

#include <QString>
#include <QList>

using Settings = std::map<QString, std::vector<uchar>>;

class MemoryMapParser
{
public:

   static Settings ParseMemoryMaps(const QList<MemoryMap>& maps, void* inputPtr, Debugger::Process proc);

   template<typename T>
   static auto GetValue(const Settings& settings, const QString& name);
   
private:

   static bool IsType(const QList<MemoryMap>& maps, const QString& name, MemoryMapType type);
   static QList<uchar> Fmt(const std::vector<uchar>& list);

   static std::vector<uchar> Range(const std::vector<uchar>& list, std::size_t offset, std::size_t size);

   static std::size_t SearchValue(const Settings& settings, const QString& str, bool* isNum = 0);
   static std::vector<uchar> Read(const Settings& settings, const QList<MemoryMap>& maps, const QString& name, const QString& offset, const QString& size, uchar* inputPtr, Debugger::Process proc, std::size_t* h_size = 0);

};

template<typename T>
auto MemoryMapParser::GetValue(const Settings& settings, const QString& name)
{
   auto data = settings.at(name);
   if constexpr (std::is_pointer<T>::value)
   {
      return MemoryFormatter::formatVec<typename std::remove_pointer<T>::type>(data);
   }
   else
   {
      return MemoryFormatter::formatVec<T>(data)[0];
   }
}
