#include "ProcessChooser.h"
#include "MemoryController.h"

#include <QTableWidgetItem>

using namespace Debugger;

namespace
{
   QString format(std::size_t bytes)
   {
      if (bytes < 1024) return QString("%1 Bytes").arg(bytes);
      if ((bytes /= 1024) < 1024) return QString("%1 KBytes").arg(bytes);
      if ((bytes /= 1024) < 1024) return QString("%1 MBytes").arg(bytes);
      if ((bytes /= 1024) < 1024) return QString("%1 GBytes").arg(bytes);
      if ((bytes /= 1024) < 1024) return QString("%1 TBytes").arg(bytes);
      return "<unknown>";
   }
}

std::size_t ProcessChooser::Choose(QWidget* parent /*= Q_NULLPTR*/)
{
   ProcessChooser chooser(parent);
   chooser.exec();
   return chooser.value;
}

ProcessChooser::ProcessChooser(QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
    loadTable();
}

void ProcessChooser::on__select_pressed()
{
   auto index = ui._table->currentRow();
   if (index < 0 || index >= pids.size()) close();
   value = pids[index];
   close();
}

void ProcessChooser::on__search_textChanged(const QString& text)
{
   for (auto i = 0; i < pids.size(); i++)
   {
      ui._table->setRowHidden(i, !ui._table->item(i, 1)->text().contains(text, Qt::CaseInsensitive));
   }
}

void ProcessChooser::loadTable()
{
   pids = MemoryController::currentProcesses();
   ui._table->setRowCount(pids.size());
   
   for (auto i = 0; i < pids.size(); i++)
   {
      auto proc = MemoryController::openProcess(pids[i]);
      auto name = MemoryController::processName(proc);
      //if (name == "<unknown>") continue;

      ui._table->setItem(i, 0, new QTableWidgetItem(QString::number(pids[i])));
      ui._table->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(name)));
      auto mods = MemoryController::getModules(proc);
      std::size_t mem = 0;
      for (auto& mod : mods) mem += mod.size;
      ui._table->setItem(i, 2, new QTableWidgetItem(format(mem)));
      MemoryController::closeProcess(proc);
   }
}
