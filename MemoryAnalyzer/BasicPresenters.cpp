#include "BasicPresenters.h"
#include "MemoryFormatter.h"
#include "MemoryController.h"

#include <QLabel>


#define BASIC_PRESENTER_DEFINITION(NAME, TYPE)																\
QString NAME::getName() const																						\
{ return #TYPE ; }																									\
QString NAME::getGroup() const																					\
{ return "Basic types" ; }																							\
QWidget* NAME::getWidget(Debugger::Process proc, void* address) const								\
{ 																															\
	auto memory = Debugger::MemoryController::read<uchar>(proc, address, sizeof(TYPE));			\
	return label(QString::number(MemoryFormatter::format<TYPE>(memory).first())); 				\
}																															\
REGISTER_PRESENTER(NAME);

namespace
{
   QWidget* label(const QString& txt)
   {
		auto lbl = new QLabel(txt);
		lbl->setAlignment(Qt::AlignCenter);
		auto font = lbl->font();
		font.setPointSize(20);
		font.setBold(true);
		lbl->setFont(font);
      return lbl;
   }
}

BASIC_PRESENTER_DEFINITION(IntPresenter, int);
BASIC_PRESENTER_DEFINITION(UIntPresenter, unsigned int);
BASIC_PRESENTER_DEFINITION(LongPresenter, long);
BASIC_PRESENTER_DEFINITION(ULongPresenter, unsigned long);
BASIC_PRESENTER_DEFINITION(LongLongPresenter, long long);
BASIC_PRESENTER_DEFINITION(ULongLongPresenter, unsigned long long);
BASIC_PRESENTER_DEFINITION(DoublePresenter, double);
BASIC_PRESENTER_DEFINITION(FloatPresenter, float);
BASIC_PRESENTER_DEFINITION(CharPresenter, char);
BASIC_PRESENTER_DEFINITION(UCharPresenter, unsigned char);