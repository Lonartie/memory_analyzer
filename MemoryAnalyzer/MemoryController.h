#pragma once

#include "Structs.h"

#include <vector>
#include <map>
#include <string>

namespace Debugger
{
	class MemoryController
	{
	public:

		static std::vector<std::size_t> currentProcesses();
		static std::size_t processOf(const void* addr);

		static std::string processName(Process process);
		static std::string processPath(Process process);

		static bool isValid(Process process);
		static Process openProcess(std::size_t pid);
		static Process openProcess(const std::string& name);
		static bool closeProcess(Process proc);
		static void enableDebugMode();
		static std::vector<MemoryModule> getModules(Process process, bool skipUnused = true);
		static void* getBaseAddress(Process proc);

		static bool addressInModules(const std::vector<MemoryModule>& modules, const void* adr);

		static std::vector<char> readMod(Process proc, const MemoryModule& info);
		template<typename T> static std::vector<T> read(Process process, void* addr, std::size_t size, std::size_t* error = 0);
		template<typename T> static std::vector<T> readUntil(Process process, void* addr, std::vector<unsigned char> end, std::size_t* error = 0);
		template<typename T> static void read(T* data_ptr, Process process, void* addr, std::size_t size, std::size_t* error = 0);

		static void* addressof(void* type, std::size_t offset);
		static void setdata(void* type, std::size_t offset, void* data);

		static std::pair<char*, std::size_t> trim(char* buffer, std::size_t size);
		static std::vector<std::pair<char*, std::size_t>> split(char* buffer, std::size_t size);

		static std::size_t getAddr(const char* ptr);
		static void* getPtr(const char* ptr);

		static std::size_t getAddr(const std::vector<char>& ptr);
		static void* getPtr(const std::vector<char>& ptr);

		template<typename T = void>
		static T* toPtr(std::size_t address);
		template<typename T>
		static std::size_t toNum(const T* address);
		template<typename T, typename D>
		static T* as(D* address);
		template<typename T, typename D>
		static const T* as(const D* address);

		static std::string toStr(const void* addr);
		static std::string toStr(std::size_t addr);

	private:

		static std::size_t lastError();
		static bool readProcessMemory(void* handle, void* addr, void* out, std::size_t size, std::size_t* read);
		static void set_privilege(void* hToken, const wchar_t* lpszPrivilege, bool bEnablePrivilege);
	};

	using MC = MemoryController;



	/************************************************************/
	/*                   TEMPLATE DEFINITIONS                   */
	/************************************************************/



	template<typename T>
	std::vector<T> Debugger::MemoryController::read(Process process, void* addr, std::size_t size, std::size_t* error /*= 0*/)
	{
		std::vector<T> result;
		result.resize(size);
		std::size_t read;

		if (!readProcessMemory(process.handle, addr, result.data(), sizeof(T) * size, &read))
		{
			if (error) *error = lastError();
			return {};
		}

		return result;
	}

	template<typename T>
	static std::vector<T> Debugger::MemoryController::readUntil(Process process, void* addr, std::vector<unsigned char> end, std::size_t* error /*= 0*/)
	{
		std::vector<T> result;
		std::vector<unsigned char> val;
		auto size = end.size();
		bool done = false;
		std::size_t offset = 0;
		std::size_t read = 0;

		while (!done)
		{
			unsigned char out = 0;
			if (!readProcessMemory(process.handle, static_cast<unsigned char*>(addr) + offset, out, 1, &read))
			{
				if (error)
					*error = lastError();
				return result;
			}
			val.push_back(out);

			// TODO: anpassen!
			//result = std::vector<T>(val.begin(), val.end());

			if (val.size() >= size)
			{
				auto off = val.end() - size;
				if (std::vector<unsigned char>(off, val.end) == end)
					return result;
			}
		}

		return {};
	}

	template<typename T>
	void Debugger::MemoryController::read(T* data_ptr, Process process, void* addr, std::size_t size, std::size_t* error /*= 0*/)
	{
		std::size_t read;
		if (!readProcessMemory(process.handle, addr, data_ptr, sizeof(T) * size, &read))
		{
			if (error) *error = lastError();
		}
	}


	template<typename T /*= void*/>
	T* Debugger::MemoryController::toPtr(std::size_t address)
	{
		return static_cast<T*>(reinterpret_cast<void*>(address));
	}

	template<typename T>
	std::size_t Debugger::MemoryController::toNum(const T* address)
	{
		return reinterpret_cast<std::size_t>(static_cast<const void*>(address));
	}

	template<typename T, typename D>
	T* Debugger::MemoryController::as(D* address)
	{
		return static_cast<T*>(static_cast<void*>(address));
	}

	template<typename T, typename D>
	const T* Debugger::MemoryController::as(const D* address)
	{
		return static_cast<const T*>(static_cast<const void*>(address));
	}
}