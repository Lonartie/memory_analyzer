#include "MemoryAnalyzer.h"
#include "MemoryFormatter.h"

#include <QCoreApplication>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QPushButton>


MemoryAnalyzer::MemoryAnalyzer(QWidget* parent)
   : QWidget(parent)
{
	m_ui.setupUi(this);

	m_ui.table->setSelectionMode(QAbstractItemView::SelectionMode::ContiguousSelection);
	m_ui.table->setSelectionBehavior(QAbstractItemView::SelectItems);
	m_ui.table->setStyleSheet("QTableWidget { selection-background-color: blue; selection-color: white; }");

   //loadData(m_input);
}

void MemoryAnalyzer::loadData(const QString& input)
{
   if (ui_IsHex())
      loadHex(input);
   else
      loadBinary(input);
}

void MemoryAnalyzer::loadHex(const QString& input)
{
   if (!MemoryFormatter::isHex(input)) return error("input is not in hex format");
   m_data = MemoryFormatter::loadHex(input);
   ui_update();
}

void MemoryAnalyzer::loadBinary(const QString& input)
{
   if (!MemoryFormatter::isBinary(input)) return error("input is not in binary format");
   m_data = MemoryFormatter::loadBinary(input);
   ui_update();
}

bool MemoryAnalyzer::ui_IsHex() const
{
   return m_ui._inputmode->currentIndex() == 0;
}

int MemoryAnalyzer::ui_watchSize() const
{
   return m_ui._watchsize->value();
}

int MemoryAnalyzer::ui_offset() const
{
   return m_ui._offset->value();
}

void MemoryAnalyzer::ui_update()
{
	QSignalBlocker block(m_ui.table);

   m_ui._bytesize->setText(QString::number(m_data.size()));
   m_ui._watchsize->setMaximum(m_data.size());
   m_ui._offset->setMaximum(m_data.size() - ui_watchSize());

   m_ui._address->setText(MemoryFormatter::concatenate(MemoryFormatter::format<MemoryFormatter::Address>(ui_scope())));
   m_ui._binary->setText(MemoryFormatter::concatenate(MemoryFormatter::format<MemoryFormatter::Binary>(ui_scope())));
   m_ui._int->setText(MemoryFormatter::concatenate(MemoryFormatter::format<int>(ui_scope())));
   m_ui._uint->setText(MemoryFormatter::concatenate(MemoryFormatter::format<unsigned int>(ui_scope())));
   m_ui._long->setText(MemoryFormatter::concatenate(MemoryFormatter::format<long>(ui_scope())));
   m_ui._ulong->setText(MemoryFormatter::concatenate(MemoryFormatter::format<unsigned long>(ui_scope())));
   m_ui._longlong->setText(MemoryFormatter::concatenate(MemoryFormatter::format<long long>(ui_scope())));
   m_ui._ulonglong->setText(MemoryFormatter::concatenate(MemoryFormatter::format<unsigned long long>(ui_scope())));
   m_ui._float->setText(MemoryFormatter::concatenate(MemoryFormatter::format<float>(ui_scope())));
   m_ui._double->setText(MemoryFormatter::concatenate(MemoryFormatter::format<double>(ui_scope())));

   int cols = 8;

   m_ui.table->clear();
   m_ui.table->setRowCount(m_data.size() / cols);
   m_ui.table->setColumnCount(cols);
   m_ui.table->horizontalHeader()->hide();
   m_ui.table->verticalHeader()->hide();

   int row = -1;
   for (int i = 0; i < m_data.size(); i++)
   {
      if (i % cols == 0) row++;
      auto txt = QString::number((int) m_data[i]);
      auto wid = new QTableWidgetItem(txt, 1);
      wid->setTextAlignment(Qt::AlignCenter);
      m_ui.table->setItem(row, i % cols, wid);
   }

   m_ui.table->resizeColumnsToContents();
   m_ui.table->resizeRowsToContents();
	m_ui.table->update();

	m_ui.table->selectionModel()->clearSelection();

	int min = ui_offset();
	int max = min + ui_watchSize() - 1;

	for (int i = min; i <= max; i++)
	{
		int row = i / cols;
		int col = i % cols;

		m_ui.table->setItemSelected(m_ui.table->item(row, col), true);
	}
}

std::vector<uchar> MemoryAnalyzer::ui_scope() const
{
   return std::vector<uchar>(m_data.begin() + ui_offset(), m_data.begin() + ui_offset() + ui_watchSize());
}

void MemoryAnalyzer::error(const QString& msg)
{
   QMessageBox::critical(this, "Error", msg);
}

void MemoryAnalyzer::on__inputmode_currentIndexChanged(int)
{
   loadData(m_input);
}

void MemoryAnalyzer::on__watchsize_valueChanged(int)
{
   loadData(m_input);
}

void MemoryAnalyzer::on__offset_valueChanged(int)
{
   loadData(m_input);
}

void MemoryAnalyzer::on_table_itemSelectionChanged()
{
	QSignalBlocker block(m_ui.table);

	auto selected = m_ui.table->selectedItems();
	auto cols = m_ui.table->model()->columnCount();

	int min = m_data.size();
	int max = 0;

	for (auto item : selected)
	{
		int row = m_ui.table->row(item);
		int col = m_ui.table->column(item);

		int index = row * cols + col;

		min = std::min(min, index);
		max = std::max(max, index);
	}

	QSignalBlocker b1(m_ui._watchsize);
	QSignalBlocker b2(m_ui._offset);

	m_ui._watchsize->setValue(max - min);
	m_ui._offset->setValue(min);

	loadData(m_input);
}

void MemoryAnalyzer::on__load_pressed()
{
   QString input;
   QDialog diag(this);
   auto layout = new QHBoxLayout;
   auto lbl = new QLabel("data: ");
   auto edi = new QPlainTextEdit();
   auto kk = new QPushButton("ok");

   connect(edi, &QPlainTextEdit::textChanged, [=, &input]() { input = edi->toPlainText(); });
   connect(kk, &QPushButton::pressed, [&diag]() { diag.close(); });

   layout->addWidget(lbl);
   layout->addWidget(edi);
   layout->addWidget(kk);

   diag.setLayout(layout);
   diag.exec();
   m_input = input;
   loadData(input);
}
