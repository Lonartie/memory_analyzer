#include "MemoryComparer.h"
#include "MemoryFormatter.h"

MemoryComparer::MemoryComparer(QWidget *parent)
    : QWidget(parent)
{
    m_ui.setupUi(this);
}

void MemoryComparer::compare(const QString& a, const QString& b)
{
   m_a = MemoryFormatter::loadHex(a);
   m_b = MemoryFormatter::loadHex(b);

   //int match = 
}

long MemoryComparer::slidingMatch(const std::vector<uchar>& a, const std::vector<uchar>& b)
{
   bool sw = (a.size() > b.size());
   auto& h = sw ? a : b;
   auto& l = sw ? b : a;

   for (std::size_t  offset = 0; offset <= h.size() - l.size(); offset++)
   {
      bool match = true;
      for (std::size_t n = 0; n < l.size(); n++)
         if (a[n] != b[n])
         {
            match = false;
            break;
         }

      if (match)
         return offset * (sw ? -1 : 1);
   }

   return std::numeric_limits<long>::quiet_NaN();;
}
