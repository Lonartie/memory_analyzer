#include <QtWidgets/QApplication>
#include <QMessageBox>
#include <QSettings>

#include "MemoryController.h"
#include "Dashboard.h"

bool IS_DEBUG_MODE_ENABLED = false;

int main(int argc, char* argv[])
{
   QApplication a(argc, argv);

//#ifdef NDEBUG
   try
   {
      Debugger::MemoryController::enableDebugMode();
      IS_DEBUG_MODE_ENABLED = true;
   } catch (...)
   {
      QSettings settings("preferences.ini", QSettings::IniFormat);
      if (!settings.value("IGNORE_INSUFICCIENT_RIGHTS").value<bool>())
      {
         auto res = QMessageBox::warning(0,
                                         "Insufficient rights",
                                         "Insufficient rights, need admin rights for some features",
                                         QMessageBox::Ignore | QMessageBox::Ok,
                                         QMessageBox::Ok);
         if (res == QMessageBox::Ignore)
         {
            settings.setValue("IGNORE_INSUFICCIENT_RIGHTS", true);
            settings.sync();
         }
      }
   }
//#endif
   
   Dashboard w;
   w.show();
   return a.exec();
}
