#pragma once

#include <QString>
#include <QWidget>

#include "MemoryPresenter.h"

#define BASIC_PRESENTER_DECLARATION(NAME)												         \
class NAME: public MemoryPresenter														         \
{																									         \
public: virtual QString getName() const;												         \
public: virtual QString getGroup() const;												         \
public: virtual QWidget* getWidget(Debugger::Process proc, void* address) const;    \
};

BASIC_PRESENTER_DECLARATION(IntPresenter); 
BASIC_PRESENTER_DECLARATION(UIntPresenter);
BASIC_PRESENTER_DECLARATION(LongPresenter);
BASIC_PRESENTER_DECLARATION(ULongPresenter);
BASIC_PRESENTER_DECLARATION(LongLongPresenter);
BASIC_PRESENTER_DECLARATION(ULongLongPresenter);
BASIC_PRESENTER_DECLARATION(DoublePresenter);
BASIC_PRESENTER_DECLARATION(FloatPresenter); 
BASIC_PRESENTER_DECLARATION(CharPresenter);
BASIC_PRESENTER_DECLARATION(UCharPresenter);