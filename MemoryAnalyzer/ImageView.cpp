#include "ImageView.h"

ImageView::ImageView(QWidget *parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);
}

void ImageView::setImage(const QImage& image)
{
	__update(image);
}

void ImageView::setImage(const QPixmap& image)
{
	__update(image.toImage());
}

void ImageView::__update(const QImage& image)
{
	m_ui.view->setImage(image);
	m_ui._width->setText(QString::number(image.width()));
	m_ui._height->setText(QString::number(image.height()));
	m_ui._format->setText(QString::number(image.format()));
}
