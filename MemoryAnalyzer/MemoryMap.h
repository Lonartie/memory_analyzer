#pragma once

#include <QString>

enum MemoryMapType
{
   POINTER, OBJECT
};

struct MemoryMap
{
   QString name;
   MemoryMapType type;
   QString content_name;
   QString offset;
   QString size;
};