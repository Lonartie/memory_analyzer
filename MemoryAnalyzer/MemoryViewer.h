#pragma once

#include <QWidget>
#include <QMap>
#include <QString>

#include "ui_MemoryViewer.h"
#include "Structs.h"
#include "QtHashes.h"

#include <vector>

class MemoryPresenter;

class MemoryViewer: public QWidget
{
   Q_OBJECT;

public:

   MemoryViewer(QWidget* parent = Q_NULLPTR);

private slots:

	void on__load_pressed();
	void on__typegroupbox_currentIndexChanged();
	void on__searchtype_textChanged(const QString& text);

private:

   std::vector<uchar> ui_address();
   void ui_update(Debugger::Process proc, const std::vector<uchar>& addr);
   MemoryPresenter* getPresenter();

   void error(const QString& msg);

	QMap<QString /*group*/, QMap<std::size_t /*index*/, QString /*type*/>> m_types;

   QString m_owner = "";
   std::size_t m_ownerpid = 0;
   std::size_t m_owersegments = 0;
   std::size_t m_ownermemory = 0;
   QWidget* m_lastwid = 0;
	QtVersion m_version;
   Ui::MemoryViewer m_ui;
};
