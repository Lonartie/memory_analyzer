#pragma once

#include "ui_MemoryComparer.h"

#include <QWidget>

#include <vector>

class MemoryComparer: public QWidget
{
   Q_OBJECT

public:
   MemoryComparer(QWidget* parent = Q_NULLPTR);

   void compare(const QString& a, const QString& b);

private:

   std::vector<uchar> m_a, m_b;
   Ui::MemoryComparer m_ui;
   
   static long slidingMatch(const std::vector<uchar>& a, const std::vector<uchar>& b);
};
