#include "MemoryFormatter.h"

namespace
{
	QList<QChar> HEX_LIST = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f'};
	QList<QChar> BIN_LIST = {'0', '1'};
}

std::vector<uchar> MemoryFormatter::loadHex(const QString& input)
{
	QString _input = QString(input).toUpper().replace("0X", "");
	std::vector<uchar> m_data;
	for (int i = (_input.size() / 2) * 2 - 2; i >= 0; i -= 2)
	{
		bool ok = false;
		uchar result = static_cast<uchar>(_input.mid(i, 2).toInt(&ok, 16));
		if (ok)
			m_data.push_back(result);
	}
	for (int i = m_data.size(); i < 8; i++)
		m_data.push_back(0);
	std::reverse(m_data.begin(), m_data.end());
	return m_data;
}

std::vector<uchar> MemoryFormatter::loadBinary(const QString& input)
{
	std::vector<uchar> m_data;
	for (int i = (input.size() / 8) * 8 - 8; i >= 0; i -= 8)
	{
		bool ok = false;
		uchar result = static_cast<uchar>(input.mid(i, 8).toInt(&ok, 2));
		m_data.push_back(result);
	}
	std::reverse(m_data.begin(), m_data.end());
	return m_data;
}

bool MemoryFormatter::isHex(const QString& input)
{
	for (const auto& ch : input)
		if (!HEX_LIST.contains(ch))
			return false;
	return true;
}

bool MemoryFormatter::isBinary(const QString& input)
{
	for (const auto& ch : input)
		if (!BIN_LIST.contains(ch))
			return false;
	return true;
}
