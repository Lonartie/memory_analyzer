#include "MemorySnapshotController.h"
#include "MemoryController.h"

#include <assert.h>
#include <algorithm>

using namespace Debugger;

GlobalSnapshot MemorySnapshotController::TakeSnapshot(Debugger::Process proc)
{
   auto mods = MemoryController::getModules(proc);
   return createSnapshot(proc, mods);
}

GlobalDifferences MemorySnapshotController::CreateDiffMap(const GlobalSnapshot& before, const GlobalSnapshot& after)
{
	GlobalSnapshot _before, _after;

	align(before, after, _before, _after);

   GlobalDifferences diff;
	diff.fallback = after;
   diff.module_count = _before.module_count;
	diff.diffs.resize(_before.module_count);
	diff.bytes = 0;
   diff.size = 0;
   std::fill(diff.diffs.begin(), diff.diffs.end(), false);
   
   for (std::size_t n = 0; n < _before.module_count; n++)
   {
      auto& snap_before = _before.modules[n];
      auto& snap_after = _after.modules[n];
      if (snap_before.begin != snap_after.begin) continue;
		auto sizemin = std::min(snap_before.size, snap_after.size);
		auto sizemax = std::max(snap_before.size, snap_after.size);

      ModuleDifferences moddiff;
      moddiff.size = 0;
      moddiff.before = snap_before;
      moddiff.after = snap_after;

      moddiff.diffs.resize(sizemax);
      std::fill(moddiff.diffs.begin(), moddiff.diffs.end(), false);

      bool has_diff = false;
      for (std::size_t i = 0; i < sizemin; i++)
      {
         if (snap_before.data[i] == snap_after.data[i]) continue;

         has_diff = true;
         moddiff.size++;
         moddiff.diffs[i] = true;
			diff.bytes++;
      }

		for (std::size_t i = sizemin; i < sizemax; i++)
		{
			moddiff.size++;
			diff.bytes++;
		}

      if (has_diff)
      {
         diff.diffs[n] = true;
         diff.size++;
      }

		diff.modules.push_back(moddiff);
   }

   return diff;
}

void MemorySnapshotController::align(const GlobalSnapshot& a, const GlobalSnapshot& b, GlobalSnapshot& _a, GlobalSnapshot& _b)
{
	std::size_t id = 0;
	
	std::vector<bool> b_done;
	std::vector<bool> a_done;

	b_done.resize(b.module_count, false);
	a_done.resize(a.module_count, false);

	_a.max_size = a.max_size;
	_b.max_size = b.max_size;

	_a.size = a.size;
	_b.size = b.size;

	for (std::size_t i = 0; i < a.module_count; i++)
	{
		for (std::size_t j = 0; j < b.module_count; j++)
		{
			if (a.modules[i].begin == b.modules[j].begin)
			{
				b_done[j] = true;
				a_done[i] = true;
				_a.modules.push_back(a.modules[i]);
				_a.module_count++;
				_b.modules.push_back(b.modules[j]);
				_b.module_count++;
			}
		}
	}

	id = 0;
	for (auto& ok : a_done)
	{
		if (!ok)
		{
			_a.modules.push_back(a.modules[id]);
			_a.module_count++;
			_b.modules.push_back({});
			_b.module_count++;
		}
		id++;
	}

	id = 0;
	for (auto& ok : b_done)
	{
		if (!ok)
		{
			_a.modules.push_back({});
			_a.module_count++;
			_b.modules.push_back(b.modules[id]);
			_b.module_count++;
		}
		id++;
	}
}

GlobalSnapshot MemorySnapshotController::createSnapshot(Debugger::Process proc, const std::vector<Debugger::MemoryModule>& mods)
{
   GlobalSnapshot snapshot;
   std::size_t err = 0;
   std::size_t max = 1e4; // hard break after max 10.000 modules
	snapshot.size = 0;
	snapshot.max_size = 0;
   for (const auto& mod : mods)
   {
      ModuleSnapshot data;
      data.data.resize(mod.size);
      data.begin = MemoryController::toNum(mod.base);
      data.size = mod.size;
		snapshot.max_size += mod.size;
      err = 0;
      MemoryController::read(data.data.data(), proc, mod.base, mod.size, &err);
      if (err) continue;
      snapshot.size += mod.size;
      snapshot.modules.push_back(data);

      if (--max == 0) break;
   }
   snapshot.module_count = snapshot.modules.size();
   return snapshot;
}
