#include "MemoryPresenter.h"
#include "MemoryViewer.h"

QString MemoryPresenter::getGroup() const
{ return ""; }

QString MemoryPresenter::getName() const
{ return "generic"; }
QWidget* MemoryPresenter::getWidget(Debugger::Process proc, void* address) const
{ return 0; }

MemoryPresenter& MemoryPresenter::instance()
{
   static MemoryPresenter __instance;
   return __instance;
}

void MemoryPresenter::RegisterPresenter(MemoryPresenter* presenter)
{
   LIST.push_back(std::move(presenter));
}

const std::vector<MemoryPresenter*>& MemoryPresenter::getRegisteredPresenters()
{
   return LIST;
}
