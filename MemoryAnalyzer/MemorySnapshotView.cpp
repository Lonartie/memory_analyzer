#include "MemorySnapshotView.h"
#include "ProcessChooser.h"
#include "MemoryController.h"
#include "MemorySnapshotAnalyzer.h"

#include <QMessageBox>

using namespace Debugger;

namespace
{
	QString format(std::size_t bytes)
	{
		if (bytes < 1024) return QString("%1 Bytes").arg(bytes);
		if ((bytes /= 1024) < 1024) return QString("%1 KBytes").arg(bytes);
		if ((bytes /= 1024) < 1024) return QString("%1 MBytes").arg(bytes);
		if ((bytes /= 1024) < 1024) return QString("%1 GBytes").arg(bytes);
		if ((bytes /= 1024) < 1024) return QString("%1 TBytes").arg(bytes);
		return "<unknown>";
	}
}

MemorySnapshotView::MemorySnapshotView(QWidget* parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);

	m_ui._addresses->setVisible(false);
	m_ui._information->setVisible(false);
	m_ui._analyzation->setVisible(false);
}

void MemorySnapshotView::on__snapshot_pressed()
{
	// first snapshot
	if (!m_has_first_snapshot)
	{
		pid = ProcessChooser::Choose(this);
		if (!pid) return;

		auto proc = MemoryController::openProcess(pid);
		before = MemorySnapshotController::TakeSnapshot(proc);
		MemoryController::closeProcess(proc);

		m_ui._snapshot->setText("Take snapshot and compare");

		m_has_first_snapshot = true;
		return;
	}

	// second snapshot
	if (!m_has_second_snapshot)
	{
		auto proc = MemoryController::openProcess(pid);
		auto after = MemorySnapshotController::TakeSnapshot(proc);

		auto diffs = MemorySnapshotController::CreateDiffMap(before, after);
		if (format(after.size) != format(after.max_size))
			m_ui._loaded->setText(QString("%1 / %2").arg(format(after.size)).arg(format(after.max_size)));
		else
			m_ui._loaded->setText(QString("%1").arg(format(after.size)));
		before = {};
		after = {};

		blocks = MemorySnapshotAnalyzer::getMemoryBlocks(diffs);

		m_ui._diffs->setText(format(diffs.bytes));
		diffs = {};

		m_ui._snapshot->setVisible(false);
		m_ui._info_1->setVisible(false);
		m_ui._info_2->setVisible(false);
		m_ui._addresses->setVisible(true);
		m_ui._information->setVisible(true);

		m_ui._pid->setText(QString::number(pid));
		m_ui._name->setText(QString::fromStdString(MemoryController::processName(proc)));

		MemoryController::closeProcess(proc);

		m_ui._analyzation->setVisible(true);
		QApplication::processEvents();
		pointers = MemorySnapshotAnalyzer::getMemoryBlockPointers(blocks, [=](float val) { m_ui._analyzation_progress->setValue(val * 100); QApplication::processEvents(); });
		m_ui._analyzation->setVisible(false);
		QApplication::processEvents();
		m_ui._blocks->setText(QString::number(blocks.size()));
		m_ui._pointers->setText(QString::number(pointers.size()));

		return;
	}
}

void MemorySnapshotView::on__update_pressed()
{
	bool ok_1 = false, ok_2 = false;
	std::size_t begin = m_ui._begin->text().replace("0x", "", Qt::CaseInsensitive).toULongLong(&ok_1, 16);
	std::size_t end = m_ui._end->text().replace("0x", "", Qt::CaseInsensitive).toULongLong(&ok_2, 16);

	if (!ok_1 || !ok_2)
	{
		QMessageBox::critical(this, "Invalid input", "Could not parse pointer addresses");
		return;
	}

	searchAddresses(begin, end);
}

void MemorySnapshotView::searchAddresses(std::size_t begin, std::size_t end)
{
	MemoryBlockChains results;
	try
	{
		results = MemorySnapshotAnalyzer::getPointerPaths(pointers, blocks, begin, end);
	} catch (const std::exception & ex)
	{
		QMessageBox::critical(this, "Exception", ex.what());
		return;
	}
	QMessageBox::information(this, "Success!", QString("Found %1 path(s)!").arg(results.size()));
	auto id = 0;
	for (auto& result : results)
	{
		QString out;
		MemoryBlockChain* current = result;
		while (current != nullptr)
		{
			out += QString("offset: %1, pointing to: %2\n").arg(current->offset).arg(current->pointer->parsed_pointer);
			current = current->next;
		}
		QMessageBox::information(this, QString("Path #%2").arg(id++), out);
	}
}
