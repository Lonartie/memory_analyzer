#pragma once

#include "ui_MemoryAnalyzer.h"

#include <QWidget>
#include <QString>

#include <vector>
#include <vector>

class MemoryAnalyzer: public QWidget
{
   Q_OBJECT

public:

   MemoryAnalyzer(QWidget* parent = Q_NULLPTR);

   void loadData(const QString& input);
   void loadHex(const QString& input);
   void loadBinary(const QString& input);

private:

   bool ui_IsHex() const;
   int ui_watchSize() const;
   int ui_offset() const;
   void ui_update();
   std::vector<uchar> ui_scope() const;

   void error(const QString& msg);

private slots:

   void on__inputmode_currentIndexChanged(int);
   void on__watchsize_valueChanged(int);
   void on__offset_valueChanged(int);
   void on_table_itemSelectionChanged();
   void on__load_pressed();

private:

   QString m_input;
   std::vector<uchar> m_data;
   Ui::MemoryAnalyzerClass m_ui;
};
