#pragma once

#include <QString>

struct QtVersion
{ int major = 0, minor1 = 0, minor2 = 0; };

class QtHashes 
{
public:
	static QtVersion GetVersion(const QString& QtCoreDllPath);
};
