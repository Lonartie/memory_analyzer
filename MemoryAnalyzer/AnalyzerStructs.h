#pragma once

#include <xutility>
#include <vector>

struct MemoryBlock
{
	std::size_t
		begin = 0,
		end = 0,
		size = 0,
		mod_offset = 0,
		mod_index = 0;

	std::vector<unsigned char> 
		data;
};

struct MemoryBlockPointer
{
	std::size_t
		source_offset = 0,
		parsed_pointer = 0,
		target_offset = 0;

	const MemoryBlock
		*source = nullptr, 
		*target = nullptr;
};

struct MemoryBlockChain
{
	const MemoryBlock
		*source = nullptr,
		*target = nullptr;

	long long
		offset;

	const MemoryBlockPointer
		*pointer = nullptr;

	MemoryBlockChain
		*next = nullptr;

	~MemoryBlockChain() { if (next) delete next; }

	MemoryBlockChain* deepClone()
	{
		auto result = new MemoryBlockChain(*this);
		if (next) result->next = next->deepClone();
		return result;
	}
};

using MemoryBlocks = std::vector<MemoryBlock>;
using MemoryBlockPointers = std::vector<MemoryBlockPointer>;
using MemoryBlockChains = std::vector<MemoryBlockChain*>;