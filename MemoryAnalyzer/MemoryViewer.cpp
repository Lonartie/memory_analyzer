#include "MemoryViewer.h"
#include "MemoryPresenter.h"

#include "MemoryFormatter.h"
#include "MemoryController.h"
#include "QtPrivateInformation.h"

#include <QMessageBox>
#include <QFileInfo>
#include <QDir>

using namespace Debugger;

extern bool IS_DEBUG_MODE_ENABLED;

namespace
{
   QString format(std::size_t byte)
   {
      if (byte < 1024) return QString("%1 Bytes").arg(byte);
      byte /= 1024;
      if (byte < 1024) return QString("%1 KBytes").arg(byte);
      byte /= 1024;
      if (byte < 1024) return QString("%1 MBytes").arg(byte);
      byte /= 1024;
      if (byte < 1024) return QString("%1 GBytes").arg(byte);
      byte /= 1024;
      if (byte < 1024) return QString("%1 TBytes").arg(byte);
      return "<unknown>";
   }
}

MemoryViewer::MemoryViewer(QWidget* parent)
   : QWidget(parent)
{
   m_ui.setupUi(this);


   {
      std::size_t i = 0;
      for (auto presenter : MemoryPresenter::instance().getRegisteredPresenters())
         if (presenter)
            if (m_types.keys().contains(presenter->getGroup()))
               m_types[presenter->getGroup()].insert(i++, presenter->getName());
            else
               m_types.insert(presenter->getGroup(), {{i++, presenter->getName()}});
         else {}
   }

   // adding groups
   for (auto i = 0; i < m_types.size(); i++)
   {
      m_ui._typegroupbox->addItem(m_types.keys()[i]);
   }

   on__typegroupbox_currentIndexChanged();

   if (!IS_DEBUG_MODE_ENABLED)
   {
      QMessageBox::warning(this, "Warning", "This application may not work because of insufficient rights!");
   }

   connect(m_ui._addressinput, &QLineEdit::returnPressed, [=]() { m_ui._searchtype->setFocus(); m_ui._searchtype->selectAll(); });
   connect(m_ui._searchtype, &QLineEdit::returnPressed, [=]() { on__load_pressed(); });
   connect(m_ui._typegroupbox->lineEdit(), &QLineEdit::returnPressed, [=]() { m_ui._presenterbox->setFocus(); m_ui._presenterbox->lineEdit()->selectAll(); });
   connect(m_ui._presenterbox->lineEdit(), &QLineEdit::returnPressed, [=]() { on__load_pressed(); });
}

void MemoryViewer::on__load_pressed()
{
   if (!(m_ui._presenterbox->currentIndex() >= 0 && m_ui._presenterbox->currentIndex() < MemoryPresenter::instance().getRegisteredPresenters().size()))
      return;

   m_ui._addressinput->setFocus();
   if (!IS_DEBUG_MODE_ENABLED)
   {
      error("Could not gather required informations because of insufficient rights!");
      return;
   }

   auto addr = ui_address();
   if (!addr.size()) return;

   void* ptr = MemoryFormatter::format<MemoryFormatter::Address>(addr).first();

   m_ownerpid = MemoryController::processOf(ptr);
   auto proc = MemoryController::openProcess(m_ownerpid);

   if (!MemoryController::isValid(proc))
   {
      error("Address could not be associated to any process...");
      return;
   }

   m_owner = QString::fromStdString(MemoryController::processName(proc));
   auto modules = MemoryController::getModules(proc);
   m_owersegments = modules.size();
   m_ownermemory = 0;

   for (const auto& mod : modules)
      m_ownermemory += mod.size;

   QFileInfo executable(QString::fromStdString(MemoryController::processPath(proc)));
   QFileInfo qt5_core_dll(executable.absoluteDir().absolutePath() + "/Qt5Core.dll");
   QFileInfo qt5_cored_dll(executable.absoluteDir().absolutePath() + "/Qt5Cored.dll");

   m_version = {};
   SetQtVersion(m_version);
   if (qt5_core_dll.exists()) m_version = QtHashes::GetVersion(qt5_core_dll.absoluteFilePath());
   if (qt5_cored_dll.exists()) m_version = QtHashes::GetVersion(qt5_cored_dll.absoluteFilePath());

   ui_update(proc, addr);
   MemoryController::closeProcess(proc);
}

std::vector<uchar> MemoryViewer::ui_address()
{
   QString input = m_ui._addressinput->text().trimmed().toUpper().replace("0X", "");

   if (input.isEmpty())
   {
      error("Input is empty");
      return {};
   }

   if (!MemoryFormatter::isHex(input))
   {
      error("Input cannot be converted to an address");
      return {};
   }

   auto result = MemoryFormatter::loadHex(input);

   return result;
}

void MemoryViewer::ui_update(Debugger::Process proc, const std::vector<uchar>& addr)
{
   m_ui._address->setText(MemoryFormatter::concatenate(MemoryFormatter::format<MemoryFormatter::Address>(addr)));
   m_ui._address_owner->setText(m_owner);
   m_ui._owner_pid->setText(QString::number(m_ownerpid));
   m_ui._owner_segment_count->setText(QString::number(m_owersegments));
   m_ui._owner_memory->setText(QString("%1").arg(format(m_ownermemory)));

   if (m_version.major != 0)
   {
      m_ui._qtversion->setText(QString("%1.%2.%3").arg(m_version.major).arg(m_version.minor1).arg(m_version.minor2));
      SetQtVersion(m_version);
   } else
   {
      m_ui._qtversion->setText("/");
   }

   auto presenter = getPresenter();
   if (!presenter)
   {
      return error("Could not get memory presenter");
   }

   if (m_lastwid) delete m_lastwid;
   m_lastwid = presenter->getWidget(proc, MemoryFormatter::format<MemoryFormatter::Address>(addr)[0]);
   if (!m_lastwid)
   {
      m_lastwid = nullptr;
      return;
   }
   m_ui.presenterContainer->layout()->addWidget(m_lastwid);
}

void MemoryViewer::on__typegroupbox_currentIndexChanged()
{
   m_ui._presenterbox->clear();
   for (auto i = 0; i < m_types.size(); i++)
   {
      if (m_ui._typegroupbox->currentIndex() == i)
      {
         for (auto j = 0; j < m_types.values()[i].size(); j++)
         {
            m_ui._presenterbox->addItem(m_types.values()[i].values()[j]);
         }
      }
   }
}

void MemoryViewer::on__searchtype_textChanged(const QString& text)
{
   for (auto i = 0; i < m_types.size(); i++)
   {
      for (auto j = 0; j < m_types.values()[i].size(); j++)
      {
         if (m_types.values()[i].values()[j].toUpper() == text.toUpper())
         {
            m_ui._typegroupbox->setCurrentIndex(i);
            m_ui._presenterbox->setCurrentIndex(j);
         }
      }
   }
}

MemoryPresenter* MemoryViewer::getPresenter()
{
   auto groupIndex = m_ui._typegroupbox->currentIndex();
   auto typeIndex = m_ui._presenterbox->currentIndex();

   if (groupIndex == -1 || typeIndex == -1) return 0;

   return MemoryPresenter::instance().getRegisteredPresenters()[m_types.values()[groupIndex].keys()[typeIndex]];
}

void MemoryViewer::error(const QString& msg)
{
   QMessageBox::critical(this, "Error", msg);
}

