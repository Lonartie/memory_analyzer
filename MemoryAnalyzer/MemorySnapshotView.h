#pragma once

#include "MemorySnapshotController.h"
#include "ui_MemorySnapshotView.h"
#include "AnalyzerStructs.h"

#include <QWidget>

class MemorySnapshotView: public QWidget
{
   Q_OBJECT

public:
   MemorySnapshotView(QWidget* parent = Q_NULLPTR);

private slots:

   void on__snapshot_pressed();
	void on__update_pressed();

private:

   void searchAddresses(std::size_t begin, std::size_t end);

	bool m_has_first_snapshot = false;
	bool m_has_second_snapshot = false;
   std::size_t pid;
   GlobalSnapshot before;
	MemoryBlocks blocks;
	MemoryBlockPointers pointers;

   Ui::MemorySnapshotView m_ui;
};
