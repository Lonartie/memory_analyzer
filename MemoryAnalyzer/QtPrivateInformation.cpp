#include "QtPrivateInformation.h"

namespace
{
	int MAJOR = 0, MINOR1 = 0, MINOR2 = 0;

	bool isSupported(int major, int minor1, int minor2)
	{
		if (MAJOR > major) return true;
		if (MAJOR < major) return false;
		if (MINOR1 > minor1) return true;
		if (MINOR1 < minor1) return false;
		if (MINOR2 > minor2) return true;
		if (MINOR2 < minor2) return false;
		return true;
	}
}

void SetQtVersion(QtVersion version)
{
	MAJOR = version.major;
	MINOR1 = version.minor1;
	MINOR2 = version.minor2;
}

QtVersion GetQtVersion()
{
	return {MAJOR, MINOR1, MINOR2};
}

/***********************************************************/
/*                      QImagePrivate                      */
/***********************************************************/

std::size_t QImagePrivate::size()
{
	if (isSupported(5, 12, 4)) return 136;

	return 136;
}

std::size_t QImagePrivate::offset_width()
{
	if (isSupported(5, 12, 4)) return 4;

	return 4;
}

std::size_t QImagePrivate::offset_height()
{
	if (isSupported(5, 12, 4)) return 8;

	return 8;
}

std::size_t QImagePrivate::offset_bytes_per_line()
{
	if (isSupported(5, 12, 4)) return 56;

	return 56;
}

std::size_t QImagePrivate::offset_format()
{
	if (isSupported(5, 12, 4)) return 48;

	return 48;
}

std::size_t QImagePrivate::offset_data()
{
	if (isSupported(5, 12, 4)) return 40;

	return 40;
}
