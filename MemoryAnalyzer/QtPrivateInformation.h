#pragma once

#include <xutility>
#include "QtHashes.h"

void SetQtVersion(QtVersion version);
QtVersion GetQtVersion();

struct QImagePrivate
{
   static std::size_t size();
   static std::size_t offset_width();
   static std::size_t offset_height();
   static std::size_t offset_bytes_per_line();
   static std::size_t offset_format();
   static std::size_t offset_data();
};