#pragma once

#include "MemorySnapshotController.h"
#include "AnalyzerStructs.h"

#include <functional>

class MemorySnapshotAnalyzer
{
public:

   static MemoryBlocks getMemoryBlocks(const GlobalDifferences& diff);
   static MemoryBlockPointers getMemoryBlockPointers(const MemoryBlocks& blocks, std::function<void(float progress)> progressFunc = nullptr);
   static MemoryBlockChains getPointerPaths(const MemoryBlockPointers& pointers, const MemoryBlocks& blocks, std::size_t begin, std::size_t end);

private:

   struct BaseData { const MemoryBlocks& blocks; const MemoryBlockPointers& pointers; };
   struct BlockFrame { const MemoryBlock* begin = nullptr, * end = nullptr; };
   struct PointersFrame { const std::vector<const MemoryBlockPointer*> begin, end; };

   static MemoryBlocks& mergeThreshold(MemoryBlocks& blocks, const GlobalSnapshot& fallback, std::size_t threshold);
   static void findRecursively(BaseData base, BlockFrame bframe, PointersFrame pframe, MemoryBlockChain* current, MemoryBlockChains& results, std::vector<const MemoryBlock*> processed = {});
};
