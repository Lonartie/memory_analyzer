#pragma once

#include <QLabel>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QImage>

class AdvancedLabel: public QLabel
{
	Q_OBJECT

public:
	AdvancedLabel(QWidget *parent = Q_NULLPTR);
	~AdvancedLabel() = default;

	void setImage(const QImage& image);
	void enableZoom(bool enable);

protected:

	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void mousePressEvent(QMouseEvent *ev) override;
	virtual void mouseMoveEvent(QMouseEvent *ev) override;
	virtual void mouseReleaseEvent(QMouseEvent *ev) override;
	virtual void wheelEvent(QWheelEvent *event) override;

private:

	void __resize(const QSize& size, const QImage& image);

	bool m_enableZoom = false;
	bool m_drag = false;
	bool m_resizeEnabled = false;
	float m_zoom = 1;
	long long m_offsetx = 0, m_offsety = 0;
	QImage m_image;
	QPoint m_mouse;
};
