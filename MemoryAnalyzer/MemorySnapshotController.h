#pragma once

#include "Structs.h"

#include <vector>

#pragma region Snapshot-Structs

struct ModuleSnapshot
{
   std::size_t 
      begin = 0,
      size = 0;

   std::vector<unsigned char>
      data;
};

struct GlobalSnapshot
{
	std::size_t
		module_count = 0,
		size = 0,
		max_size = 0;

   std::vector<ModuleSnapshot>
      modules;
};

struct ModuleDifferences
{
   ModuleSnapshot
      before,
      after;

   std::size_t
      size = 0;

   std::vector<bool>
      diffs;
};

struct GlobalDifferences
{
   std::size_t
      module_count = 0,
      size = 0,
		bytes = 0;

   std::vector<ModuleDifferences>
      modules;

   GlobalSnapshot
      fallback;

   std::vector<bool>
      diffs;
};

#pragma endregion

class MemorySnapshotController
{
public:

   static GlobalSnapshot TakeSnapshot(Debugger::Process proc);
   static GlobalDifferences CreateDiffMap(const GlobalSnapshot& before, const GlobalSnapshot& after);

private:

	static void align(const GlobalSnapshot& a, const GlobalSnapshot& b, GlobalSnapshot& _a, GlobalSnapshot& _b);
   static GlobalSnapshot createSnapshot(Debugger::Process proc, const std::vector<Debugger::MemoryModule>& mods);
};
