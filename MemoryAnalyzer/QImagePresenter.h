#pragma once

#include "MemoryPresenter.h"

class QImagePresenter : public MemoryPresenter
{
public:
	virtual QString getName() const override;
	virtual QString getGroup() const override;
	virtual QWidget* getWidget(Debugger::Process proc, void* address) const override;
};
