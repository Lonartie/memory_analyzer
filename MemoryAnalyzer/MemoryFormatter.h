#pragma once

#include <QString>
#include <QList>
#include <QVector>

#include <vector>
#include <list>

class MemoryFormatter 
{
public:

   using Binary = bool;
   using Address = void*;

   static std::vector<uchar> loadHex(const QString& input);
   static std::vector<uchar> loadBinary(const QString& input);

   static bool isHex(const QString& input);
   static bool isBinary(const QString& input);
     
   template<typename T, typename = typename std::enable_if<!std::is_same<T, bool>::value && !std::is_same<T, void*>::value>::type>
   static QList<T> format(const std::vector<uchar>& memory, int mode = {});

   template<typename T, typename = typename std::enable_if<std::is_same<T, bool>::value>::type>
   static QList<T> format(const std::vector<uchar>& memory, bool mode = {});

   template<typename T, typename = typename std::enable_if<std::is_same<T, void*>::value>::type>
   static QList<T> format(const std::vector<uchar>& memory, void* mode = {});

   template<typename T, typename = typename std::enable_if<!std::is_same<T, bool>::value && !std::is_same<T, void*>::value>::type>
   static std::vector<T> formatVec(const std::vector<uchar>& memory, int mode = {});

   template<typename T, typename = typename std::enable_if<std::is_same<T, bool>::value>::type>
   static std::vector<T> formatVec(const std::vector<uchar>& memory, bool mode = {});

   template<typename T, typename = typename std::enable_if<std::is_same<T, void*>::value>::type>
   static std::vector<T> formatVec(const std::vector<uchar>& memory, void* mode = {});

   template<typename T, typename = typename std::enable_if<!std::is_same<T, bool>::value && !std::is_same<T, void*>::value>::type>
   static QString concatenate(const QList<T>& data, int mode = {});

   template<typename T = bool, typename = typename std::enable_if<std::is_same<T, bool>::value>::type>
   static QString concatenate(const QList<T>& data, bool mode = {});

   template<typename T = bool, typename = typename std::enable_if<std::is_same<T, void*>::value>::type>
   static QString concatenate(const QList<T>& data, void* mode = {});

	template<typename T, typename D>
	static long long offsetOf(T* obj, D* target);

};



/******************************************************************/
/*                      TEMPLATE DEFINITIONS                      */
/******************************************************************/



template<typename T, typename>
static QList<T> MemoryFormatter::format(const std::vector<uchar>& memory, int)
{
   std::vector<uchar> _memory(memory);
   auto size = _memory.size() / sizeof(T);
   const T* num = static_cast<const T*>(static_cast<const void*>(&_memory[0]));
   return QList<T>::fromStdList(std::list<T>(num, num + size));
}

template<typename T, typename>
static QList<T> MemoryFormatter::format(const std::vector<uchar>& memory, bool)
{
   std::vector<uchar> _memory(memory);
   QList<T> result;
   for (auto num : _memory)
      for (auto ch : QString::number(num, 2).rightJustified(8, '0'))
         result << QString(ch).toInt();
   return result;
}

template<typename T, typename>
static QList<T> MemoryFormatter::format(const std::vector<uchar>& memory, void*)
{
   auto rev(memory);
   std::reverse(rev.begin(), rev.end());
   auto list = format<std::size_t>(rev);
   QList<T> result;
   for (auto& i : list) result << reinterpret_cast<T>(i);
   return result;
}

template<typename T, typename>
static std::vector<T> MemoryFormatter::formatVec(const std::vector<uchar>& memory, int)
{
   std::vector<uchar> _memory(memory);
   auto size = _memory.size() / sizeof(T);
   const T* num = static_cast<const T*>(static_cast<const void*>(&_memory[0]));
   return std::vector<T>(num, num + size);
}

template<typename T, typename>
static std::vector<T> MemoryFormatter::formatVec(const std::vector<uchar>& memory, bool)
{
   std::vector<uchar> _memory(memory);
   std::vector<T> result;
   for (auto num : _memory)
      for (auto ch : QString::number(num, 2).rightJustified(8, '0'))
         result.push_back(QString(ch).toInt());
   return result;
}

template<typename T, typename>
static std::vector<T> MemoryFormatter::formatVec(const std::vector<uchar>& memory, void*)
{
   auto rev(memory);
   std::reverse(rev.begin(), rev.end());
   auto list = format<std::size_t>(rev);
   QList<T> result;
   for (auto& i : list) result.push_back(reinterpret_cast<T>(i));
   return result;
}


template<typename T, typename>
QString MemoryFormatter::concatenate(const QList<T>& data, int)
{
   if (!data.size()) return "/";

   QString result;
   for (const auto& dat : data) result += QString::number(dat) + "\n";
   return result.trimmed();
}

template<typename T, typename>
QString MemoryFormatter::concatenate(const QList<T>& data, bool)
{
   if (!data.size()) return "/";

   QString result;
   int i = 0;
   for (const auto& dat : data) result += QString::number(dat) + (i % 8 == 0 && i % 16 != 0 ? " " : i % 16 == 0 ? "\n" : "") + (i++?"":"");
   return result.trimmed();
}

template<typename T, typename>
QString MemoryFormatter::concatenate(const QList<T>& data, void*)
{
   if (!data.size()) return "/";

   QString result;
   for (auto i : data) result += "0x" + QString::number(reinterpret_cast<std::size_t>(i), 16).rightJustified(16, '0').toUpper() + "\n";
   return result.trimmed();
}

template<typename T, typename D>
long long MemoryFormatter::offsetOf(T* obj, D* target)
{
	std::size_t a0 = reinterpret_cast<std::size_t>(static_cast<void*>(target));
	void* _a0 = &a0;

	std::vector<uchar> ab((uchar*) obj, (uchar*) obj + sizeof(T));
	std::vector<uchar> a0b((uchar*) _a0, (uchar*) _a0 + sizeof(_a0));

	for (std::size_t offset = 0; offset < (ab.size() - a0b.size()); offset++)
	{
		bool equal = true;
		for (std::size_t i = 0; i < a0b.size(); i++)
			if (ab[offset + i] != a0b[i])
			{
				equal = false;
				break;
			}
		if (equal)
			return offset;
	}
	return -1;
}
