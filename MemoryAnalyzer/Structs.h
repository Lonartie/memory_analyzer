#pragma once

#include <xutility>

namespace Debugger
{
   enum State
   { Commit, Reserve, Free };

   enum Type
   { Image, Mapped, Private };

   enum Protection
   { Read, ReadWrite, WriteCopy, Execute, ExecuteRead, ExecuteReadWrite, ExecuteWriteCopy };

   struct MemoryModule
   {
      void* base;
      std::size_t size;

      State state;
      Type type;
      Protection protection;

      bool guard;
      bool nocache;
   };

   struct Process
   {
      void* handle = nullptr;
      std::size_t pid = 0;
   };
}