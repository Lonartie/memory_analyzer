#include "MemoryViewer.h"
#include "Dashboard.h"
#include "MemoryAnalyzer.h"
#include "MemoryComparer.h"
#include "MemorySnapshotView.h"

#include <QToolButton>
#include <QTabWidget>
#include <QTabBar>
#include <QLabel>
#include <QDialog>
#include <QComboBox>
#include <QPushButton>

Dashboard::Dashboard(QWidget* parent)
   : QWidget(parent)
{
   m_ui.setupUi(this);
   connect(m_ui.addBtn, &QPushButton::pressed, this, &Dashboard::addTab);
   connect(m_ui.tabWidget, &QTabWidget::tabCloseRequested, this, &Dashboard::closeTab);
	m_ui.tabWidget->addTab(new MemoryViewer(), "Memory Viewer");
}

void Dashboard::addTab()
{
   auto widgets = selectWidget();
   
   for (auto wid : widgets)
      m_ui.tabWidget->addTab(wid, wid->objectName());

   m_ui.tabWidget->setCurrentIndex(m_ui.tabWidget->count() - 1);
}

void Dashboard::closeTab(int pos)
{
	delete m_ui.tabWidget->widget(pos);
   m_ui.tabWidget->removeTab(pos);
}

QList<QWidget*> Dashboard::selectWidget()
{
   QDialog diag(this);
   diag.setWindowTitle("Select a type");
   auto layout = new QHBoxLayout();

   auto lbl = new QLabel("Type: ");
   auto cmb = new QComboBox();
   auto exi = new QPushButton("ok");

   cmb->addItems({"Analyze memory", "View memory", "Memory snapshot"});
   cmb->setCurrentIndex(0);
   int selected = 0;

   connect(cmb, qOverload<int>(&QComboBox::currentIndexChanged), [&](int ind) { selected = ind; });
   connect(exi, &QPushButton::pressed, [&]() { diag.close(); });

   layout->addWidget(lbl);
   layout->addWidget(cmb);
   layout->addWidget(exi);

   diag.setLayout(layout);
   diag.exec();

   QList<QWidget*> out;

   switch (selected)
   {
   case 0:
   {
      auto wid = new MemoryAnalyzer();
      wid->setObjectName("Memory Analyzer");
      out << wid;
      break;
   }
   case 1:
   {
      auto wid = new MemoryViewer();
      wid->setObjectName("Memory Viewer");
      out << wid;
      break;
   }
   case 2:
   {
      auto wid = new MemorySnapshotView();
      wid->setObjectName("Memory Snapshot");
      out << wid;
      break;
   }
   }

   return out;
}