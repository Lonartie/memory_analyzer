#pragma once

#include "MemoryPresenter.h"

#define VECTOR_PRESENTER_DECLRATION(NAME)                                                    \
class NAME: public MemoryPresenter                                                           \
{                                                                                            \
public: virtual QString getName() const override;                                            \
public: virtual QString getGroup() const override;                                           \
public: virtual QWidget* getWidget(Debugger::Process proc, void* address) const override;    \
};

VECTOR_PRESENTER_DECLRATION(VectorIntPresenter);
VECTOR_PRESENTER_DECLRATION(VectorUIntPresenter);
VECTOR_PRESENTER_DECLRATION(VectorLongPresenter);
VECTOR_PRESENTER_DECLRATION(VectorULongPresenter);
VECTOR_PRESENTER_DECLRATION(VectorLongLongPresenter);
VECTOR_PRESENTER_DECLRATION(VectorULongLongPresenter);
VECTOR_PRESENTER_DECLRATION(VectorCharPresenter);
VECTOR_PRESENTER_DECLRATION(VectorUCharPresenter);
VECTOR_PRESENTER_DECLRATION(VectorDoublePresenter); 
VECTOR_PRESENTER_DECLRATION(VectorFloatPresenter);