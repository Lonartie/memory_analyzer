#pragma once

#include <QWidget>
#include "ui_Dashboard.h"

class Dashboard: public QWidget
{
   Q_OBJECT

public:
   Dashboard(QWidget* parent = Q_NULLPTR);
   ~Dashboard() = default;

private slots:

   void addTab();
   void closeTab(int pos);

private:

   QList<QWidget*> selectWidget();

   Ui::Dashboard m_ui;
};
