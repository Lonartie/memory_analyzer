#include "MemoryMapParser.h"
#include "MemoryFormatter.h"

#include <QFile>

using namespace Debugger;

Settings MemoryMapParser::ParseMemoryMaps(const QList<MemoryMap>& maps, void* inputPtr, Debugger::Process proc)
{
   Settings settings;
   auto _input = static_cast<uchar*>(inputPtr);
   auto num = MemoryController::toNum(_input);
   uchar* numptr = static_cast<uchar*>(static_cast<void*>(&num));
   settings.emplace("input", std::vector<uchar>(numptr, numptr + sizeof(std::size_t)));
   auto _maps = maps;
   _maps.prepend({"input", POINTER, "", "0", "8"});

   for (const auto& map : maps)
   {
      auto name = map.name;
      auto other_name = map.content_name;
      auto type = map.type;
      std::size_t size = 0;
      auto data = Read(settings, _maps, other_name, map.offset, map.size, _input, proc, &size);
      settings.emplace(name, data);
   }

   return settings;
}

bool MemoryMapParser::IsType(const QList<MemoryMap>& maps, const QString& name, MemoryMapType type)
{
   for (const auto& map : maps)
      if (map.name == name)
         return map.type == type;
   return false;
}

QList<uchar> MemoryMapParser::Fmt(const std::vector<uchar>& list)
{
   QList<uchar> out;
   for (const auto& ch : list) out << ch;
   return out;
}

std::vector<uchar> MemoryMapParser::Range(const std::vector<uchar>& list, std::size_t offset, std::size_t size)
{
   return std::vector<uchar>(list.begin() + offset, list.begin() + offset + size);
}

std::size_t MemoryMapParser::SearchValue(const Settings& settings, const QString& str, bool* isNum /*= 0*/)
{
   if (str.isEmpty()) return 0;

   bool ok = false;
   std::size_t size = str.toULongLong(&ok);

   if (ok)
   {
      if (isNum) *isNum = true;
      return size;
   }

   QList<QString> keys;
   for (const auto& kvp : settings) keys << kvp.first;
   if (keys.contains(str))
   {
      if (isNum) *isNum = false;
      auto list = settings.at(str);
      return MemoryFormatter::format<std::size_t>(list)[0];
   }

   throw std::runtime_error("could not find data");
}

std::vector<uchar> MemoryMapParser::Read(const Settings& settings, const QList<MemoryMap>& maps, const QString& name, const QString& offset, const QString& size, uchar* inputPtr, Debugger::Process proc, std::size_t* h_size)
{
   bool isNum = false;
   std::size_t t_offset = SearchValue(settings, offset);
   std::size_t t_size = SearchValue(settings, size, &isNum);

   if (IsType(maps, name, POINTER))
   {
      uchar* ptr = 0;

      if (name == "input")
         ptr = inputPtr;
      else
         ptr = static_cast<uchar*>(MemoryController::toPtr(MemoryFormatter::format<std::size_t>(settings.at(name))[0]));

      if (isNum)
      {
         if (h_size) *h_size = t_size;
         auto data = MemoryController::read<uchar>(proc, ptr + t_offset, t_size);
         return data;
      } 
      else
      {
         auto begin = ptr + t_offset;
         auto end = static_cast<uchar*>(MemoryController::toPtr(t_size));
         auto c_size = end - begin;

         if (h_size) *h_size = c_size;
         auto data = MemoryController::read<uchar>(proc, begin, c_size);
         return data;
      }
   }
   else if(IsType(maps, name, OBJECT))
   {
      auto data = settings.at(name);
      auto range = Range(data, t_offset, t_size);
      return range;
   }
   return {};
}