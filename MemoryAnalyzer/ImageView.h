#pragma once

#include <QWidget>
#include <QImage>
#include <QPixmap>

#include "ui_ImageView.h"

class ImageView : public QWidget
{
	Q_OBJECT

public:
	ImageView(QWidget *parent = Q_NULLPTR);
	~ImageView() = default;

	void setImage(const QImage& image);
	void setImage(const QPixmap& image);

private:
	
	void __update(const QImage& image);
	
	Ui::ImageView m_ui;
};
