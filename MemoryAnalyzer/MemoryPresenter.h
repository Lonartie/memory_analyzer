#pragma once

#include <QWidget>
#include <QString>

#include "Structs.h"

#include <memory>
#include <vector>

class MemoryPresenter
{
public:

	virtual QString getGroup() const;
   virtual QString getName() const;
   virtual QWidget* getWidget(Debugger::Process proc, void* address) const;

   static MemoryPresenter& instance();

   void RegisterPresenter(MemoryPresenter* presenter);
   const std::vector<MemoryPresenter*>& getRegisteredPresenters();

protected:

   MemoryPresenter() = default;
   MemoryPresenter(const MemoryPresenter&) = default;
   MemoryPresenter(MemoryPresenter&&) = default;
   ~MemoryPresenter() = default;

private:

   std::vector<MemoryPresenter*> LIST;

};

#define REGISTER_PRESENTER(TYPE) \
bool TYPE##_registered = []() { MemoryPresenter::instance().RegisterPresenter(new TYPE()); return true; } ();


